# Modern CI/CD
This repository is for setting and describing the challenges that are offered for those who learn CI/CD initialized and guidelined of Yoram Michaeli.

## Challanges

- [GitLab challange](challanges/gitlab-challange)
- [GitHub Actions challange](challanges/github-actions-challange)
- [ArgoCD challange](challanges/argocd-challange)
